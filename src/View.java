public class View {

    public static final String WRONG_INPUT_DATA_HELLO = "Wrong input! Repeat please! Must be  String - 'Hello' ";
    public static final String WRONG_INPUT_DATA_WORLD = "Wrong input! Repeat please! Must be String - 'World!' ";

    public static final String INPUT_DATA_HELLO = "Input 'Hello' = ";
    public static final String INPUT_DATA_WORLD = "Input 'World!' = ";

    public static final String WRONG_INPUT_DATA = "Wrong input! Repeat please! ";


    public static final String OUR_OUTPUT = " Output is = ";

    public void printMessage(String message){
        System.out.println(message);
    }

    public void printMessageAndString(String message, String value){
        System.out.println(message + value);
    }
}

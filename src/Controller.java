import java.util.Scanner;
import java.util.regex.Pattern;

public class Controller {

    public static final String HELLO_FINAL = "Hello";
    public static final String WORLD_FINAL = "World!";
    //public static final int FOUR = 4;
    private Model model;
    private View view;

    // Constructor
    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }


    Pattern patternHello = Pattern.compile(HELLO_FINAL);
    Pattern patternWorld = Pattern.compile(WORLD_FINAL);

    // The Work method
    public void processUser() {
        Scanner sc = new Scanner(System.in);


        model.setData(inputIntValueWithScanner(sc));
        view.printMessageAndString(View.OUR_OUTPUT, model.getData());
    }

    // The Utility methods
    public String inputIntValueWithScanner(Scanner sc) {
        view.printMessage(View.INPUT_DATA_HELLO);
        String outputFromScanner = "";

        boolean flag = false;
        while (!flag) {

            if (sc.hasNext(patternHello)) {


                String s1 = sc.nextLine();

                outputFromScanner = sc.nextLine(); // new string
                outputFromScanner += " ";
                view.printMessage(View.INPUT_DATA_WORLD);

                while(!flag) {
                    if (sc.hasNext(patternWorld)) {
                        String s2 = sc.nextLine();
                        outputFromScanner += sc.nextLine();
                        flag = true;
                    } else {
                        view.printMessage(View.WRONG_INPUT_DATA_WORLD + View.INPUT_DATA_WORLD);
                        sc.next();
                    }
                }


            } else {
                view.printMessage(View.WRONG_INPUT_DATA_HELLO + View.INPUT_DATA_HELLO);
                sc.next();
            }
        }

        return outputFromScanner;
    }
}
